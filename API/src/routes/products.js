const express = require('express');
const api = express.Router();
const productController = require('../controllers/productController');
const reviewController = require('../controllers/reviewController');
const authMiddleware = require('../middleware/authMiddleware');
const roleMiddleware = require('../middleware/roleMiddleware');

// Get all products
api.get('/listproducts', productController.getAllProducts);

// Get a specific product
api.get('/:id', productController.getProductById);

// Create a new product (admin only)
api.post('/createproduct', productController.createProduct)//, authMiddleware, roleMiddleware('admin'), productController.createProduct);

// Update a product (admin only)
api.put('/:id', authMiddleware, roleMiddleware('admin'), productController.updateProduct);

// Delete a product (admin only)
api.delete('/delete/:id', /*authMiddleware, roleMiddleware('admin'),*/ productController.deleteProduct);

// Get reviews for a product
api.get('/:productId/reviews', reviewController.getReviewsForProduct);

// Create a new review (admin only)
api.post('/:productId/reviews', authMiddleware, roleMiddleware('admin'), reviewController.createReview);

// Update a review (admin only)
api.put('/reviews/:id', authMiddleware, roleMiddleware('admin'), reviewController.updateReview);

// Delete a review (admin only)
api.delete('/reviews/:id', authMiddleware, roleMiddleware('admin'), reviewController.deleteReview);

module.exports = api;
