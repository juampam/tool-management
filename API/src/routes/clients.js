const express = require('express');
const userController = require('../controllers/userController');
const bodyParser = require('body-parser');
const api = express.Router()
const authMiddleware = require('../middleware/authMiddleware');


api.use(bodyParser.json());

api.use(express.json())
api.post('/register', userController.saveClient); 
api.post('/login', userController.loginUser );
api.post('/products', authMiddleware, userController.associateUserWithProduct);
api.put('/products/:id', authMiddleware, userController.updateRating);
api.get('/listusers', userController.getUsers)
module.exports = api;
