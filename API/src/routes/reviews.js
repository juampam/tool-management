const express = require('express');
const reviewController = require('../controllers/reviewController');
const bodyParser = require('body-parser');
const authMiddleware = require('../middleware/authMiddleware');
const api = express.Router()

api.use(bodyParser.json());

api.use(express.json())
 
api.get('/listreviews', reviewController.getAllReviews)
api.get('/findbyp/:productId', reviewController.getReviewsForProduct);  
api.post('/newreview', reviewController.createReview);
api.put('/updatereview/:id', reviewController.updateReview);
api.delete('/removerv/:id', reviewController.deleteReview);

module.exports = api;
