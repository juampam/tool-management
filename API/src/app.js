const dotenv = require('dotenv')
dotenv.config()
const cors = require('cors');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const secret = process.env.JWT_SECRET;
const corsOptions = {
	origin: ['http://10.100.11.78:5173', 'http://localhost:5173'], // Replace with your Svelte app's origin
	optionsSuccessStatus: 200, // Some legacy browsers (IE11, various SmartTVs) choke on 204
	credentials: true // Allow credentials (cookies, authorization headers, etc.)
}
//  middlewares

app.use(bodyParser.json())
app.use(cors(corsOptions));


//  routes

const userRoutes = require('./routes/clients');
app.use('/api/users', userRoutes); 

const productRoutes = require('./routes/products'); 
app.use('/api/products', productRoutes);

const reviewRoutes = require('./routes/reviews');
app.use('/api/reviews', reviewRoutes);


// Start server
const port = 3000;
app.listen(port, () => {
  console.log(`Server running on port ${port}`);
  console.log(secret)
});
