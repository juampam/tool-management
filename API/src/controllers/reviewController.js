const db = require('../database/db');

exports.getReviewsForProduct = (req, res) => {
  const productId = req.params.productId;
  const query = `
    SELECT Reviews.id, Reviews.review_text, Users.name AS created_by
    FROM Reviews
    JOIN Users ON Reviews.created_by = Users.id
    WHERE Reviews.product_id = ?
  `;
  db.query(query, [productId], (err, result) => {
    if (err) throw err;
    res.status(200).json(result);
  });
};

exports.createReview = (req, res) => {
  const { product_id } = req.params;
  const { review_text, created_by } = req.body;
  const query = 'INSERT INTO Reviews (product_id, review_text, created_by) VALUES (?, ?, ?)';
  const values = [product_id, review_text, created_by];
  db.query(query, values, (err, result) => {
    if (err) throw err;
    res.status(201).json({ message: 'Review created successfully' });
  });
};

exports.updateReview = (req, res) => {
  const reviewId = req.params.id;
  const { review_text } = req.body;
  const query = 'UPDATE Reviews SET review_text = ? WHERE id = ?';
  const values = [review_text, reviewId];
  db.query(query, values, (err, result) => {
    if (err) throw err;
    if (result.affectedRows === 0) {
      return res.status(404).json({ message: 'Review not found' });
    }
    res.status(200).json({ message: 'Review updated successfully' });
  });
};

exports.deleteReview = (req, res) => {
  const reviewId = req.params.id;
  const query = 'DELETE FROM Reviews WHERE id = ?';
  db.query(query, [reviewId], (err, result) => {
    if (err) throw err;
    if (result.affectedRows === 0) {
      return res.status(404).json({ message: 'Review not found' });
    }
    res.status(200).json({ message: 'Review deleted successfully' });
  });
}

exports.getAllReviews = (req, res) => {
  const  query = 'SELECT * FROM Reviews';
  db.query(query, (error, results) => {
    if (error) throw error;
    res.status(200).json(results);
  });
};


