const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const db = require('../database/db');

exports.saveClient = async  (req, res) => {
 const {
    name,
    email,
	 	password,
	 	role,
	 	phone,
    address
  	} = req.body; 
  const hashedPassword = await bcrypt.hash(password, 10);

  const query = `
    INSERT INTO Users (name, email, password, role, phone, address) 
    VALUES (?, ?, ?, ?, ?, ?)
  `;

  console.log(req.body); // Check req.body again after extracting values

  db.query(query, [name, email, hashedPassword, role, phone, address], (err, results) => {
    if (err) {
      console.error(err);
      res.status(500).send('Error saving client');
    } else {
      res.status(200).json({results});
    }
  });
};


exports.loginUser = async (req, res) => {
  try {
    const { email, password } = req.body;
    const query = 'SELECT * FROM Users WHERE email = ?';
      db.query(query, [email], async (err, result) => {
      if (err) throw err;
      if (result.length === 0) {
        return res.status(404).json({ message: 'User not found' });
      }
      const user = result[0];
      const isPasswordValid = await bcrypt.compare(password, user.password);
      if (!isPasswordValid) {
        return res.status(401).json({ message: 'Invalid password' });
      }
      const token = jwt.sign({ userId: user.id, role: user.role }, process.env.JWT_SECRET, { expiresIn: '1h' });
	res.status(200).json({ token });
    });
  } catch (err) {
    console.log(err.message)
    res.status(500).json({ message: err.message });
  }
};

exports.associateUserWithProduct = (req, res) => {
  const { user_id, product_id, rating } = req.body;
  const query = 'INSERT INTO UserProducts (user_id, product_id, rating) VALUES (?, ?, ?)';
  const values = [user_id, product_id, rating || null];
  db.query(query, values, (err, result) => {
    if (err) throw err;
    res.status(201).json({ message: 'User associated with product successfully' });
  });
};

exports.updateRating = (req, res) => {
  const associationId = req.params.id;
  const { rating } = req.body;
  const query = 'UPDATE UserProducts SET rating = ? WHERE id = ?';
  const values = [rating, associationId];
  db.query(query, values, (err, result) => {
    if (err) throw err;
    if (result.affectedRows === 0) {
      return res.status(404).json({ message: 'User-product association not found' });
    }
    res.status(200).json({ message: 'Rating updated successfully' });
  });
}

exports.getUsers = (req, res) =>{
	const query = 'SELECT * FROM Users'
	db.query(query,(err,result) => {
		if(err) throw err;
		if(result.affectedRows === 0 ){
			return res.status(404).json({message: 'No Users'})
		}
		res.status(200).json(result)
	})
}
