const db = require('../database/db');

exports.getAllProducts = (req, res) => {
  const query = 'SELECT * FROM Products';
  db.query(query, (err, result) => {
    if (err) throw err;
    res.status(200).json(result);
  });
};

exports.getProductById = (req, res) => {
  const productId = req.params.id;
  const query = 'SELECT * FROM Products WHERE id = ?';
  db.query(query, [productId], (err, result) => {
    if (err) throw err;
    if (result.length === 0) {
      return res.status(404).json({ message: 'Product not found' });
    }
    res.status(200).json(result[0]);
  });
};

exports.createProduct = (req, res) => {
  const { name, brand, image_url } = req.body;
  const query = 'INSERT INTO Products (name, brand, image_url) VALUES (?, ?, ?)';
  const values = [name, brand, image_url];
  db.query(query, values, (err, result) => {
    if (err) throw err;
    res.status(201).json({ message: 'Product created successfully' });
  });
};

exports.updateProduct = (req, res) => {
  const productId = req.params.id;
  const { name, brand, image_url } = req.body;
  const query = 'UPDATE Products SET name = ?, brand = ?, image_url = ? WHERE id = ?';
  const values = [name, brand, image_url, productId];
  db.query(query, values, (err, result) => {
    if (err) throw err;
    if (result.affectedRows === 0) {
      return res.status(404).json({ message: 'Product not found' });
    }
    res.status(200).json({ message: 'Product updated successfully' });
  });
};

exports.deleteProduct = (req, res) => {
  const productId = req.params.id;
  const query = 'DELETE FROM Products WHERE id = ?';
  db.query(query, [productId], (err, result) => {
    if (err) throw err;
    if (result.affectedRows === 0) {
      return res.status(404).json({ message: 'Product not found' });
    }
    res.status(200).json({ message: 'Product deleted successfully' });
  });
};
